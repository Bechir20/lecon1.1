Question1 : 


Cette instruction est une projection, on ne sélectionne que certains attributs d'une table.

Question2: 


Cette instruction est une restriction, on pose une condition sur les données à récupérer. Ici, il s'agit de récupérer uniquement les lignes pour lesquelles le nom est Dupont.

Question3 : 


Il s'agit d'un produit car il associe à chaque étudiant toutes les tEtu de la table tUv.
Question4: 


c'est une jointure car elle permet d'associer des lignes de 2 tables en associant l’égalité des valeurs d’une colonne d’une première table par rapport à la valeur d’une colonne d’une seconde table.

Question 5: 

La jointure est la composition d'un produit et d'une restriction puisqu'on peut d'abord effectuer un produit et ensuite ne conserver que les lignes qui satisfont à l'égalité.